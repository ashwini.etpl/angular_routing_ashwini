var myApp = angular.module("myApp", ['ui.router']);
var base_url = "http://" + window.location.hostname + "angular_routing_demo";
myApp.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.when("", "/main_home");
    $urlRouterProvider.otherwise("/main_home");
    $stateProvider.state("/main_home", {
        url: "/main_home",
        views: {
            'main_home': {
                templateUrl: "main_home.html"
            }
        }
    }).state("/main_home.home", {
        url: "/home",
        views: {
            'content': {
                templateUrl: "home.html"
            }
        }
    }).state("/main_home.about", {
        url: "/about",
        views: {
            'content': {
                templateUrl: "about.html"
            }
        }
    }).state("/main_home.gallery", {
        url: "/gallery",
        views: {
            'content': {
                templateUrl: "gallery.html"
            }
        }
    }).state("/main_home.contact", {
        url: "/contact",
        views: {
            'content': {
                templateUrl: "contact.html"
            }
        }
    }).state("/main_home.gallery.product", {
        url: "/product",
        views: {
            'prodcontent': {
                templateUrl: "product.html",
                controller: function ($scope) {
                    $scope.products = [{name: 'Shirt'}, {name: 'Jeans'}, {name: 'Watch'}, {name: 'Bags'}];
                }
            }
        }
    });
});